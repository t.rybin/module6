public class MaxInArray {
    public static void main(String[] args) {
        double[] numbers = new double[]{16, 98.4, 28, 1329, 605, 11};
        findSecondMaximum(numbers);
    }
    public static void findSecondMaximum(double[] numbers){
        boolean isSorted = false;
        double secondMax = 0;
        while(!isSorted){
            isSorted= true;
            for(int i = 0; i< numbers.length-1; i++) {
                if (numbers[i] > numbers[i + 1]) {
                    isSorted = false;
                    double buf = numbers[i];
                    numbers[i] = numbers[i + 1];
                    numbers[i + 1] = buf;
                }
                secondMax = numbers[numbers.length-2];
            }
        }
        System.out.println("The second maximum element is: "+ secondMax);
    }

}
