import java.util.Random;

public class Gct {
    public static void main(String[] args) {
        int firstNumber = 1285;
        int secondNumber = 4625;
        gcdAndScm(firstNumber, secondNumber);
    }
    public static void gcdAndScm(int firstNumber, int secondNumber){
        int gcd=0;
        for (int i =1; i<= firstNumber && i<= secondNumber; i++){
            if (firstNumber%i==0 && secondNumber%i==0){
                gcd = i;
            }
        }
        int scm = (firstNumber*secondNumber)/gcd;
        System.out.printf("GCD of %d and %d is: %d", firstNumber, secondNumber, gcd);
        System.out.printf("%n SCM of %d and %d is: %d", firstNumber, secondNumber, scm);
    }
        }
