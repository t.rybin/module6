public class FourNumbers {
    public static void main(String[] args) {
        int firstNumber = 8642;
        int secondNumber = 1337;
        int thirdNumber = 228;
        int fourthNumber = 7;
        greatestCommonDivisor(firstNumber, secondNumber, thirdNumber, fourthNumber);
    }
    public static void greatestCommonDivisor(int firstNumber, int secondNumber, int thirdNumber, int fourthNumber){
        int gcd=0;
        for (int i =1; i<= firstNumber && i<= secondNumber && i<=thirdNumber && i<= fourthNumber; i++){
            if (firstNumber%i==0 && secondNumber%i==0 && thirdNumber%i==0 && fourthNumber%i==0){
                gcd = i;
            }
        }
        System.out.printf("GCD of %d, %d, %d and %d is: %d", firstNumber, secondNumber,thirdNumber, fourthNumber, gcd);
    }
}
