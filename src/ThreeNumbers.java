public class ThreeNumbers {
    public static void main(String[] args) {
        int firstNumber = 26;
        int secondNumber = 132;
        int thirdNumber = 1000;
        smallestCommonMultiple(firstNumber, secondNumber, thirdNumber);
    }
    public static int gcd(int firstNumber, int secondNumber){
        int gcd=0;
        for (int i =1; i<= firstNumber && i<= secondNumber; i++){
            if (firstNumber%i==0 && secondNumber%i==0){
                gcd = i;
            }
        }
        return gcd;
    }
    public static int scm(int firstNumber, int secondNumber){
        return (firstNumber*secondNumber)/gcd(firstNumber, secondNumber);
    }
    public static void smallestCommonMultiple(int firstNumber, int secondNumber, int thirdNumber){
        int scm1 = scm(firstNumber, secondNumber);
        int scmGeneral = scm(scm1, thirdNumber);
        System.out.printf("SCM of %d, %d and %d is: %d", firstNumber, secondNumber, thirdNumber, scmGeneral);
    }
}
